import cv2
import numpy as np
import pandas as pd

while(True):
    img = cv2.imread('tmn-1.jpg')
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    lr = int(input())
    lb = int(input())
    lg = int(input())
    ur = int(input())
    ub = int(input())
    ug = int(input())
    lower = np.array([lb, lg, lr])
    upper = np.array([ub, ug, ur])
    mask = cv2.inRange(hsv, lower, upper)
    cv2.imshow('gambar', mask)
    cv2.waitKey(0)
