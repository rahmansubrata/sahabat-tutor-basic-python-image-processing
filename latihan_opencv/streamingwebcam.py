import cv2
import numpy as np
import pandas as pd
# Panggil image
vid = cv2.VideoCapture('http:192.168.189.29:4747/video')
while(True):
    ret, frame = vid.read()
    # pengolahan citra
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lower_green = np.array([30, 30, 30])
    upper_green = np.array([255, 255, 250])
    mask = cv2.inRange(hsv, lower_green, upper_green)
    res = cv2.bitwise_and(frame, frame, mask=mask)
    # menghitung luasan citra
    pixel_putih = np.sum(mask == 255)
    print('jml Pixel putih=', pixel_putih)
    # perhtiungan rgb
    # r = []
    # g = []
    # b = []
    # for line in res:
    #     for p in line:
    #         r.append(p[2])
    #         g.append(p[1])
    #         b.append(p[0])
    # pixels = pd.DataFrame({'red': r,
    #                        'green': g,
    #                        'blue': b
    #                        })
    # x = [i for i in r if i > 0]
    # y = [j for j in g if j > 0]
    # z = [k for k in b if k > 0]
    # sum_x = sum(x)
    # sum_y = sum(y)
    # sum_z = sum(x)
    # red = sum_x/len(x)
    # green = sum_y/len(y)
    # blue = sum_z/len(z)
    # print('R=', red, ',B=', green, ',G=', blue)
    # Menampilkan citra
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

vid.release()
cv2.destroyAllWindows()
