import cv2
import numpy as np
import pandas as pd
# Panggil image
img = cv2.imread("a/tmn-1.jpg")
# pengolahan citra
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
lower_green = np.array([30, 30, 30])
upper_green = np.array([255, 255, 250])
mask = cv2.inRange(hsv, lower_green, upper_green)
res = cv2.bitwise_and(img, img, mask=mask)
# menghitung luasan citra
pixel_putih = np.sum(mask == 255)
print('jml Pixel putih=', pixel_putih)
# perhtiungan rgb
r = []
g = []
b = []
for line in res:
    for p in line:
        r.append(p[2])
        g.append(p[1])
        b.append(p[0])
pixels = pd.DataFrame({'red': r,
                       'green': g,
                       'blue': b
                       })
x = [i for i in r if i > 0]
y = [j for j in g if j > 0]
z = [k for k in b if k > 0]
sum_x = sum(x)
sum_y = sum(y)
sum_z = sum(x)
red = sum_x/len(x)
green = sum_y/len(y)
blue = sum_z/len(z)
print('luas pixel=', pixel_putih)
print('R=', red, ',B=', green, ',G=', blue)
# Menampilkan citra
cv2.imshow('frame', mask)
cv2.waitKey(0)
