# membuat screenshot dari IP camera Otomatis Penamaan
import cv2
import datetime
import time
import numpy as np
import os
cam = cv2.VideoCapture('http://192.168.189.29:8080')
# cam=cv2.VideoCapture(0)
#    cv2.namedWindow("Streaming IP camera")
img_counter = 0
while True:
    ret, frame = cam.read()
    # merubah jadi biner
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lower_green = np.array([100, 70, 60])
    upper_green = np.array([255, 255, 255])
    mask = cv2.inRange(hsv, lower_green, upper_green)
    res = cv2.bitwise_and(frame, frame, mask=mask)
    jml_pixputih = np.sum(mask == 255)
    if not ret:
        print("gagal tampilkan kamera")
        break
    # tampilkanstreaming kamera
    cv2.imshow("Streaming IP camera", frame)
    #cv2.imshow("Streaming IP camera", crop)
    #cv2.imshow("Streaming IP camera",undistorted_img_mask_crop)
    #cv2.imshow("Streaming IP camera", undistorted_img_mask_crop)
    #cv2.imshow("Streaming IP camera", frame)

    k = cv2.waitKey(1)
    if k % 256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    elif k % 256 == 32:
        # SPACE pressed
        img_name = "opencv_frame_{}.png".format(img_counter)
        # inisiasi penamaan sampel
        # waktu pengambilan
        tday = datetime.date.today()
        # lokasi lakukan inisiasi awal
        lokasi = 'PF'
        # nomor sampel--nanti di input lewat keyboard
        print("masukan nama sampel:")
        sampel = input()
        Filename = lokasi + '-' + str(tday) + '-' + sampel

        # lokasi penyimpanan gambar
        Lokasi = 'gh-30'
        #Lokasi = parent_dir + '/' + directory
        print(Lokasi)
        Simpan = Lokasi + '/'+'ORIGINAL-' + Filename + '.jpg'
        Simpan1 = Lokasi + '/' + 'RES-' + '-' + Filename + '.jpg'
        Simpan2 = Lokasi + '/'+'BINER-' + \
            str(jml_pixputih)+'-' + Filename + '.jpg'

        # simpan : cv2.imwrite(penamaan file, nama file yang digunakan)
        cv2.imwrite(Simpan, frame)
        cv2.imwrite(Simpan2, mask)
        cv2.imwrite(Simpan1, mask)

        print(Simpan)
        img_counter += 1

cam.release()
cv2.destroyAllWindows()
