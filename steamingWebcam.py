import cv2
import numpy as np

# cap = cv2.VideoCapture('rtsp://192.168.189.29:8080/h264_pcm.sdp')
cap = cv2.VideoCapture(0)


while(True):
    ret, frame = cap.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lower_green = np.array([0, 0, 0])
    upper_green = np.array([255, 255, 250])
    mask = cv2.inRange(hsv, lower_green, upper_green)
    res = cv2.bitwise_and(frame, frame, mask=mask)
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
