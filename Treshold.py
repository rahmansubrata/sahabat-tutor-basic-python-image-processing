import cv2
import numpy as np
img = cv2.imread("src/tmn-1.jpg")
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
lower_green = np.array([30, 30, 30])
upper_green = np.array([255, 255, 250])
mask = cv2.inRange(hsv, lower_green, upper_green)
res = cv2.bitwise_and(img, img, mask=mask)
cv2.imshow('frame', res)
cv2.waitKey(0)
