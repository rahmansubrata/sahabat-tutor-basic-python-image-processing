# Sahabat Tutor-Basic Python Image Procesing

## Getting started

Selamat Datang Di Pelatihan Series From Zero To Hero Programmers For The Basics of The Startup Ecosystem
Basic Arduino Simulation - Basic Web Programmer - Basic Python Image Procesing

Basic Web library Codeigniter 4-Basic Spatial Analyst with ArcGis
presented by sahabattutor.com

## About Sahabat Tutor Training Series

Pelatihan ini diperuntukan bagi orang-orang yang ingin belajar dasar-dasar menjadi programmer hebat di masa datang. (note pelatihan ini bersipat basic tidak cocok untuk yang tingkat advance atau programmer lanjutan). Pelatihan dilakukan secara online melalui aplikasi zoom meeting.

Output dari pelatihan ini yaitu diharapkan menjadi dasar yang kuat bagi pemula yang akan belajar programming sehingga dapat peserta diharapkan dapat mengembangkan dasar pengetahuannya untuk dunia programmer. Peserta pelatihan mampu untuk melakukan penyelesaian dari error sehingga mampu belajar dengan cepat dari banyak tutorial yang sudah ada di internet maupun youtube.

## Description

Pelatihan diadakan secara online pada 28 agustus 2021 sampai 18 September 2021

Pelatihan ini terdiri dari beberapa series yang bisa diikuti

1. Pelatihan simulation control otomatic basic simulation Arduino with proteus.
2. Pelatihan basic web programmer with php for backend end bootstrap for frontend , and database mysql
3. Pelatihan Basic programmer python for image processing with opencv library image for data scient
4. Pelatihan Basic Spasial Analiyst with ArcGis
5. Pelatihan basic code for fullstack developer with framework Codeigniter 4

## Installation

1. Download python  
   https://www.python.org/downloads/

2. Download library opencv, numpy, pandas
   pip install opencv-python
   pip install numpy
   pip install pandas

3. install droidcam
https://play.google.com/store/apps/details?id=com.dev47apps.droidcam&hl=en&gl=US

3. jalankan program

4. petunjuk video instalasi
https://youtu.be/LxuSvTe-Ffw

## Usage

Souce code pelatihan dasar programmer untuk dapat melakukan pengolahan data citra sederhana menjadi data yang dapat diolah serta sreaming menggunakan smartphone
## Contributing

thankyou for all contributed
Rahman Subrata as trainer
Tri Abdi Fauzi as trainer
Galang Yusal Farisi as Project manejer

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

Slide Teknologi robotik-Teknik Pertanian dan biosistem IPB
https://www.python.org
https://opencv.org/
https://play.google.com/store/apps/details?id=com.dev47apps.droidcam&hl=en&gl=US



## More infromation

more information
http://sahabattutor.com/pelatihan
https://play.google.com/store/apps/details?id=id.codepanda.sahabattutor.tutor&hl=en&gl=US

information
Join with sahabattutor as tutor
ipb.link/panduan-pendaftaran-pengajar-st
