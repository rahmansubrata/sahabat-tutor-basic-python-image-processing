import cv2
import numpy as np

cap = cv2.VideoCapture('http://192.168.19.167:4747/video')
# out = cv2.VideoWriter('output.avi', -1, 20.0, (640, 480))
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi', fourcc, 20.0,
                      (int(cap.get(3)), int(cap.get(4))))
while(True):
    ret, frame = cap.read()
    # hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # lower_green = np.array([39, 43, 22])
    # upper_green = np.array([255, 255, 250])
    # mask = cv2.inRange(hsv, lower_green, upper_green)
    # res = cv2.bitwise_and(frame, frame, mask=mask)
    if ret == True:
        # write the flipped frame
        out.write(frame)

        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break
cap.release()
out.release()
cv2.destroyAllWindows()
