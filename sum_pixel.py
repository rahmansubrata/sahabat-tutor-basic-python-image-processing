import numpy as np
import cv2
import cv2
import numpy as np
img = cv2.imread("src/tmn-1.jpg")
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
lower_green = np.array([30, 30, 30])
upper_green = np.array([255, 255, 250])
mask = cv2.inRange(hsv, lower_green, upper_green)
res = cv2.bitwise_and(img, img, mask=mask)
number_of_white_pix = np.sum(mask == 255)
print(number_of_white_pix)
cv2.imshow('frame', mask)
cv2.waitKey(0)
